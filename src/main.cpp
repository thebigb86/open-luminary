#include <hw_memmap.h>
#include <sysctl.h>
#include <gpio.h>

void delay(unsigned long cycles);

class Blinky
{
public:
	void ledInit();

	void ledToggle();

private:
	bool ledState;
};

void Blinky::ledInit()
{
	SysCtlPeripheralEnable(PWM0_PERIPH);

	GPIODirModeSet(PWM0_PORT, PWM0_PIN, GPIO_DIR_MODE_OUT);
	GPIOPinTypeGPIOOutput(PWM0_PORT, PWM0_PIN);
}

void Blinky::ledToggle()
{
	this->ledState = !this->ledState;
	GPIOPinWrite(PWM0_PORT, PWM0_PIN, this->ledState);
}

Blinky blinky;

int main()
{
	blinky.ledInit();

	while (1)
	{
		blinky.ledToggle();
		delay(300000);
	}
}

void delay(unsigned long cycles)
{
	for (unsigned long i = 0; i < cycles; i++)
	{
		__asm("nop");
	}
}