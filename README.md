# Open Luminary

## Project
This is a basic setup to get the LM3S6965 evaluation kit up and running under Linux with the CLion IDE.


## Tooling/libraries

 - [GNU Tools for ARM Embedded Processors](https://launchpad.net/gcc-arm-embedded)
 - [OpenOCD](http://openocd.sourceforge.net/)
 - [CMake](http://www.cmake.org/)
 - [JetBrains CLion IDE](http://blog.jetbrains.com/clion/)
 - [StellarisWare® Driver Library Standalone Package](http://www.ti.com/tool/sw-drl) (a reorganized version)

## Working environment
The package has been tested to work on the following:

- VMware Workstation 10 virtual machine with USB pass-through of the Luminary device
- [Xubuntu 14.04.1](http://nl.archive.ubuntu.com/ubuntu-cdimage-xubuntu/releases/14.04/release/) (3.13.0-36 kernel)
- [GNU Tools for ARM Embedded Processors 4.8-2014-q2-update](https://launchpad.net/gcc-arm-embedded/4.8/4.8-2014-q2-update/+download/gcc-arm-none-eabi-4_8-2014q2-20140609-linux.tar.bz2)
- [OpenOCD 0.8.0](http://sourceforge.net/projects/openocd/files/openocd/0.8.0/openocd-0.8.0.tar.gz/download)
- [CLion EAP build 138.2344.17](http://download.jetbrains.com/cpp/clion-138.2344.17.tar.gz)

## Getting started

### The easy way

If you don't run Ubuntu, set up a virtual machine with the latest Ubuntu or Xubuntu. USB pass-through was verified to work with VMware Workstation 10.
Grab one of the following downloads and follow the instructions in the `HOWTO INSTALL.txt` file.

- [Installation package **with** dependencies](https://bitbucket.org/thebigb86/open-luminary/downloads/ek-lm3s6965-dev-kit-1.01.tar.gz)
- [Installation package **without** dependencies](https://bitbucket.org/thebigb86/open-luminary/downloads/ek-lm3s6965-dev-kit-no-pkgs-1.01.tar.gz)

### I like doing things the hard way

#### General

Make sure `/usr/local/bin` is in the `PATH` environment variable.

#### Manual dependencies
	
- **OpenOCD**  
    http://sourceforge.net/projects/openocd/files/openocd/0.8.0/openocd-0.8.0.tar.gz/download


- **GNU Tools for ARM Embedded Processors**  
    https://launchpad.net/gcc-arm-embedded/4.8/4.8-2014-q2-update/+download/gcc-arm-none-eabi-4_8-2014q2-20140609-linux.tar.bz2


- **JetBrains CLion (tested with 138.2344.17 EAP)** - optional  
    http://download.jetbrains.com/cpp/clion-138.2344.17.tar.gz


#### Required Ubuntu packages

	g++
	cmake 
	libc6:i386 
	libgcc1:i386 
	gcc-4.6-base:i386 
	libstdc++5:i386
	libstdc++6:i386
	libftdi-dev
	libftdi1
	libusb-1.0.0
  
   You will also need the following packages if you will be using CLion:
    
	openjdk-7-jdk
	icedtea-7-plugin

If you will be using CLion and have installed the `openjdk-7-jdk` package, you may have to update the java alternatives:

	sudo update-java-alternatives -s `sudo update-java-alternatives -l | grep 1.7.0 | awk '{print $1;}'`

#### Installing GNU Tools for ARM Embedded Processors

If you have downloaded the linked package above, it should be pretty straightforward. Just unpack the archive, move, and link the binaries.

	tar -xvf gcc-arm-none-eabi-*.tar.bz2
	sudo mv gcc-arm-none-eabi-*/ /usr/local/bin
	sudo ln -s /usr/local/bin/gcc-arm-none-eabi-*/bin/* /usr/local/bin

#### Building and installing OpenOCD

Use `--enable-legacy-ft2232_libftdi` and `--enable-ti-icdi` as flags for `./configure`. Other than that, standard procedure.

	tar -xvf openocd-*.tar.gz
	cd openocd-*/
	./configure --enable-legacy-ft2232_libftdi --enable-ti-icdi
	make
	sudo make install
	cd ..

#### Standard peripheral library

From the [installation package without dependencies](https://bitbucket.org/thebigb86/open-luminary/downloads/ek-lm3s6965-dev-kit-no-pkgs-1.01.tar.gz) copy the `stellaris-stdlib` directory to a desired location, e.g.:

	sudo cp -R stellaris-stdlib /usr/local/lib

If you do not use `/usr/local/lib`, don't forget to change the `ST_LIB_ROOT` variable in the `CMake/Modules/FindStellaris-Standard-Library.cmake` file or the CMake cache.

#### Patched board configuration

In the OpenOCD `boards` directory create a new file with the following contents:

	source [find interface/ftdi/luminary.cfg]
	set WORKAREASIZE 0x5000
	set CHIPNAME lm3s6965
	source [find target/stellaris.cfg]

Then change the `BOARD_FLASH_CONFIG` variable in the `CMakeLists.txt` file or the CMake cache to match the created file.

*The default path for the `boards` directory is `/usr/local/shared/openocd/scripts/boards`*

#### Making OpenOCD usable from non-root accounts

Add your user to the `plugdev` group:

	sudo useradd -G plugdev $USER

Create a new `.rules` file in `/etc/udev/rules.d` with the following contents:

	ATTRS{idVendor}=="0403", ATTRS{idProduct}=="bcd9", MODE="666", GROUP="plugdev"

Where `0403` is the vendor ID for the USB device and `bcd9` is the model ID. Other vendor and model IDs can be identified with the `lsusb` command.

Finally reload the `udev` rules:

	sudo udevadm control --reload-rules
	sudo udevadm trigger

**DONE**

**You should be ready to go!**